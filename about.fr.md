---
layout: page
permalink: /about/
title: "À propos"
show_title: false
lang: fr
---

{% include image.html url="/images/GTR-Palmenhaus-400px.jpg" width=200 style="float:right" %}

Je suis directeur de l'équipe de recherche en cultures intérieure et cultures intelligentes et chercheur chez [Biopterre](https://biopterre.com){:target="_blank"  rel="noopener noreferrer"}. Mon équipe travaille sur les sytèmes de culture intelligents (agrophotonique, culture en milieu fermé, utilisation de l'IA) et sur la production de métabolites secondaires dans les plantes (culture de cannabis et molécules pour l'industrie pharmaceutique).

J'ai été jusqu'à récemment affilié au groupe d'écophysiologie végétale à l'[Institut de la botanique](http://www.dib.boku.ac.at/institut-fuer-botanik-botany/){:target="_blank"  rel="noopener noreferrer"} de l'[Université des ressources naturelles et sciences de la vie (BOKU Wien)](http://boku.ac.at){:target="_blank"  rel="noopener noreferrer"}, où j'ai été le chercheur principal d'un projet financé par le [WWTF](wwtf.at){:target="_blank"  rel="noopener noreferrer"} sur l'[anatomie dynamique des feuilles](https://wwtf.at/programmes/life_sciences/#LS19){:target="_blank"  rel="noopener noreferrer"}. J'ai été récipiendaire de la bourse [Lise-Meitner](https://fr.wikipedia.org/wiki/Lise_Meitner){:target="_blank"  rel="noopener noreferrer"} du [Austrian Science Fund (FWF)](http://www.fwf.ac.at/en/){:target="_blank"  rel="noopener noreferrer"}. De 2014 à 2017, j'étais boursier du [Katherine Esau Postdoctoral Fellowship](http://www-plb.ucdavis.edu/esau/info/postdoc.htm){:target="_blank"  rel="noopener noreferrer"} à l'Université de la Californie à Davis, dans le laboratoire de [Matthew Gilbert](http://gilbertlab.ucdavis.edu/){:target="_blank"  rel="noopener noreferrer"}.

 Mes travaux de recherche en biologie portent sur la compréhension des bases physiologiques, anatomiques et structurelles de l'efficacité de l'utilisation des ressources, principalement en lien avec la lumière et l'eau au niveau de la feuille. L'objectif final de mes travaux est de mieux prédire comment les plantes feront face aux variations climatiques. Mon expérience est vaste et mes travaux ont traité de sujet fondamentaux et appliqués en physiologie végétales, tant en chambre de croissance qu'à l'intérieur de projets en plein champ. Mes travaux inclus une forte composante technologique, utilisant des techniques diverses, et n'hésitant pas à contruire de l'équipement spécifique, afin de prendres des mesures sur des plantes et dans le sol, et ainsi intégrer ces données dans des modèles à l'échelle de la feuille, du plant, ou du champ.

Mes principaux collaborateurs des dernières années sont [Danny Tholen (BOKU)](https://scholar.google.com/citations?user=iQUjOxAAAAAJ&hl=en&num=20&oi=ao){:target="_blank"  rel="noopener noreferrer"} et [Adam Roddy (Florida International University)](https://www.adamroddy.com/){:target="_blank"  rel="noopener noreferrer"}.

Au sein du projet financé par le WWTF, je collabore avec:  
  [Ingeborg Lang (biologiste cellulare, Université de Vienne)](https://scholar.google.com/citations?user=8tzh90wAAAAJ&hl=en&num=20&oi=ao){:target="_blank"  rel="noopener noreferrer"}  
  [Walter Kropatsch et Jiří Hladůvka (informaticiens, groupe "pattern recongnition and image analysis", TU Wien)](https://www.prip.tuwien.ac.at/){:target="_blank"  rel="noopener noreferrer"}  
  [Anja Geitmann (biologiste cellulaire, McGill University)](https://www.plantbiomechanics.net/){:target="_blank"  rel="noopener noreferrer"}  
  [Anne Bonnin (physicienne, Swiss Light Source)](https://www.psi.ch/en/x-ray-tomography-group/people/anne-bonnin){:target="_blank"  rel="noopener noreferrer"}


<!-- Mes autres collaborateurs actuels sont:  
  [Carlos Herrera, BOKU Wien](https://scholar.google.com/citations?user=lBAuLl0AAAAJ&hl=en&num=20&oi=sra)  
   [Craig Brodersen, Yale University](http://campuspress.yale.edu/brodersenlab/)  
   [Mason Earles, UC Davis](http://jmearles.net/)  
   [Elisabeth Forrestel, UC Davis](https://scholar.google.com/citations?user=w8sjBOcAAAAJ&hl=en&num=20&oi=ao)  
   [Mina Momayyezi, UC Davis](https://scholar.google.com/citations?user=IcktHCsAAAAJ&hl=en&num=20&oi=ao)  
   [Chris Muir, U. of Hawai'i](http://cdmuir.netlify.com/)  
   [Kevin Simonin, San Francisco State University](https://www.kevinsimonin.com/) 


Sans oublié mes collaborateurs plus ou moins récents :  
   [Kevin Boyce, Stanford University](https://earth.stanford.edu/kevin-boyce)  
   [Matthew Gilbert, UC Davis](http://gilbertlab.ucdavis.edu/)  
   [Or Sperling, ARO-Volcani Center, Gilat, Israel](http://www.agri.gov.il/en/people/1254.aspx)  
   [Maciej Zwieniecki, UC Davis](http://www.plantsciences.ucdavis.edu/plantsciences_faculty/zwieniecki/)  
   Gilbert Éthier, Université Laval  
   [Steeve Pepin, Université Laval](https://scholar.google.com/citations?hl=en&user=pvl_4L8AAAAJ)  
   [Line Rochefort, Université Laval](http://www.gret-perg.ulaval.ca/)  
   [Line Lapointe, Université Laval](https://scholar.google.com/citations?hl=en&user=GtCAwxgAAAAJ)


 -->
