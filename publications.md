---
layout: page
permalink: /publications/
title: "Publications"
lang: en
---

*: Equal contribution among authors / Contribution égale entre auteurs

<!-- 
# Preprints
 -->

# Published
[Legacy effects of past water abundance shape trees 7-years later.](https://doi.org/10.1002/ajb2.16452){:target="_blank"  rel="noopener noreferrer"} Chin ARO, Gessler A, Lain-Lacosta O, Østerlund I, Schaub M, **Théroux-Rancourt G**, Voggeneder K, Hille Ris Lambers J. (2025)  *American Journal of Botany* e16452. [*free full text*](https://onlinelibrary.wiley.com/share/author/WEQ2XNRHJWKQK78CJJWT?target=10.1002/ajb2.16452){:target="_blank"  rel="noopener noreferrer"}

[Into the spongy-verse: structural differences between leaf and flower mesophyll.](https://doi.org/10.1093/icb/icae154){:target="_blank"  rel="noopener noreferrer"} Schreel JDM, **Théroux-Rancourt G**, Diggle PK, Brodersen CR, Roddy AB. (2024) *Integrative and Comparative Biology* Icae154.

[Linking foliar structure to function: why and how.](https://doi.org/10.1071/FP24150){:target="_blank"  rel="noopener noreferrer"} (2024) Schreel JDM, **Théroux-Rancourt G**, Roddy AB.*Functional Plant Biology* 51, FP24150.

[Analyzing anatomy over three dimensions unpacks the differences in mesophyll diffusive area between sun and shade Vitis vinifera leaves.](https://academic.oup.com/aobpla/advance-article/doi/10.1093/aobpla/plad001/7005251){:target="_blank"  rel="noopener noreferrer"} **Théroux-Rancourt G**, Herrera C, Voggeneder K, Luijken N, Nocker L, Savi T, Scheffknecht S, Schneck M, Tholen D. (2023) *AoB Plants* 15, plad001.

[Localized growth drives spongy mesophyll morphogenesis.](https://royalsocietypublishing.org/eprint/RIQRG5CAG7CMJHC962ZU/full){:target="_blank"  rel="noopener noreferrer"} Treado JD, Roddy AB, **Théroux-Rancourt G**, Zhang L, Ambrose C, Brodersen C, Shattuck MD, O’Hern CS. (2022) *Journal of the Royal Society Interface*. 19, 20220602. [*preprint version*](https://arxiv.org/abs/2208.08800){:target="_blank"  rel="noopener noreferrer"} 

[Desiccation of the leaf mesophyll and its implications for CO<sub>2</sub> diffusion and light processing.](https://doi.org/10.1111/pce.14287){:target="_blank"  rel="noopener noreferrer"} Momayyezi M, Borsuk A, Brodersen CR, Gilbert ME, **Théroux-Rancourt G**, Kluepfel D, McElrone A. (2022)  *Plant, Cell & Environment*. 45, 1362–1381.

[Structural organization of the spongy mesophyll.](https://nph.onlinelibrary.wiley.com/doi/10.1111/nph.17971) Borsuk AM, Roddy AB, **Théroux-Rancourt G**, Brodersen CR. (2022) *New Phytologist* 234, 946-960. [*free full text*](https://onlinelibrary.wiley.com/share/author/SFMRE8XMQ83MYBTYYBDF?target=10.1111/nph.17971){:target="_blank"  rel="noopener noreferrer"} [*preprint version*](https://www.biorxiv.org/content/10.1101/852459v2){:target="_blank"  rel="noopener noreferrer"}

[The 3D construction of leaves is coordinated with water use efficiency in conifers.](https://doi.org/10.1111/nph.17772){:target="_blank"  rel="noopener noreferrer"} Trueba S, **Théroux-Rancourt G**, Earles JM, Buckley TN, Love D, Johnson DM, Brodersen CR. (2022) *New Phytologist*. 233, 851–861. [*free full text*](https://onlinelibrary.wiley.com/share/author/9XEWTZDARZ4ZHJHVRTNC?target=10.1111/nph.17772){:target="_blank"  rel="noopener noreferrer"} [*preprint*](https://www.biorxiv.org/content/10.1101/2021.04.23.441113v1){:target="_blank"  rel="noopener noreferrer"}

[Understanding Airspace in Leaves: 3D Anatomy and Directional Tortuosity](https://onlinelibrary.wiley.com/doi/10.1111/pce.14079){:target="_blank"  rel="noopener noreferrer"} Harwood R\*, **Théroux-Rancourt G\***, Barbour MM. (2021) *Plant, Cell & Environment*. 44, 2455-2465. *[free full text](https://onlinelibrary.wiley.com/share/author/DPQAQKKG2MNRS5KQEUAP?target=10.1111/pce.14079){:target="_blank"  rel="noopener noreferrer"}*

[Maximum CO<sub>2</sub> diffusion inside leaves is limited by the scaling of cell size and genome size.](https://royalsocietypublishing.org/doi/10.1098/rspb.2020.3145){:target="_blank"  rel="noopener noreferrer"} **Théroux-Rancourt G\***, Roddy AB\*, Earles JM, Simonin K, Zwieniecki MA, Boyce CK, Tholen D, McElrone A, Brodersen CR. (2021)  *Proceedings B of the Royal Society* 288, 20203145.

>This paper in the news:   
>[Warum es für Blattzellen richtig war, im Lauf der Kreidezeit zu schrumpfen](https://www.falter.at/heureka/20210331/warum-es-fuer-blattzellen-richtig-war--im-lauf-der-kreidezeit-zu-schrumpfen/_5c4c7265ae){:target="_blank"  rel="noopener noreferrer"} (*Falter*, March 31st, 2021)  
>[Erbe aus der Kreidezeit machte heutiges Pflanzenwachstum möglich.](https://www.derstandard.at/story/2000124518987/erbe-aus-der-kreidezeit-ermoeglicht-heutiges-wachstumgeschrumpfte-pflanzenzellen-verbesserten-die){:target="_blank"  rel="noopener noreferrer"} (*Der Standard*, February 27, 2021)  
>[Miniaturisierung von Pflanzenzellen verbesserte CO<sub>2</sub>-Aufnahme.](https://science.apa.at/power-search/7056587236869476845){:target="_blank"  rel="noopener noreferrer"} (*Austrian Press Agency*, February 24, 2021)

[A meta-analysis of mesophyll conductance to CO<sub>2</sub> in relation to major abiotic stresses in poplar species.](https://academic.oup.com/jxb/advance-article/doi/10.1093/jxb/erab127/6178815){:target="_blank"  rel="noopener noreferrer"} Elferjani R, Benomar L, Momayyezi M, Tognetti R, Niinemets Ü, Soolanayakanahally R, **Théroux-Rancourt G**, Tosens T, Lamara M, Ripullone F, Bilodeau-Gauthier S, Lamhamedi M, Calfapietra C. *Journal of Experimental Botany*, 2021, erab127.

[Digitally deconstructing leaves in 3D using X-ray microcomputed tomography and machine learning.](https://bsapubs.onlinelibrary.wiley.com/doi/full/10.1002/aps3.11380){:target="_blank"  rel="noopener noreferrer"} **Théroux-Rancourt G\***, Jenkins MR\*, Brodersen CR, McElrone AJ, Forrestel EJ, Earles JM (2020) *Applications in Plant Sciences* 8(7), e11380.

[Assessing adaptive and plastic responses in growth and functional traits in a 10-year old common garden experiment with pedunculate oak (Quercus robur L.) suggests that directional selection can drive climatic adaptation.](https://onlinelibrary.wiley.com/doi/10.1111/eva.13034){:target="_blank"  rel="noopener noreferrer"} George JP, **Théroux-Rancourt G**, Rungwattana K, Scheffknecht S, Momirovic N, Neuhauser L, Weißenbacher L, Watzinger A, Hietz P (2020) *Evolutionary Applications* 13, 2422–2438.

[Shape matters: the pitfalls of analyzing mesophyll anatomy.](https://nph.onlinelibrary.wiley.com/doi/10.1111/nph.16360){:target="_blank"  rel="noopener noreferrer"} **Théroux-Rancourt G\***, Voggeneder K, Tholen D\* (2020) *New Phytologist* 225(6), 2239–2242.

[The scaling of genome size and cell size limits maximum rates of photosynthesis with implications for ecological strategies.](https://www.journals.uchicago.edu/doi/abs/10.1086/706186){:target="_blank"  rel="noopener noreferrer"} Roddy AB, **Théroux-Rancourt G**, Abbo T, Benedetti JW, Brodersen CR, Castro M, Castro S, Gilbride AB, Jensen B, Jiang GF, Perkins JA, Perkins SD, Loureiro J, Syed Z, Thompson RA, Kuebbing SE, Simonin KA (2020) *International Journal of Plant Sciences 181*, 75–87.

[Beyond porosity: 3D leaf intercellular airspace traits that impact mesophyll conductance.](http://www.plantphysiol.org/content/178/1/148){:target="_blank"  rel="noopener noreferrer"} Earles JM\*, **Théroux-Rancourt G\***, Gilbert ME, McElrone A, Brodersen C (2018) *Plant Physiology* 178(1), 148–162. doi: 10.1104/pp.18.00550

 [The bias of a 2D view: Comparing 2D and 3D mesophyll surface area estimates using non-invasive imaging of internal leaf structure.](http://onlinelibrary.wiley.com/doi/10.1111/nph.14687/abstract){:target="_blank"  rel="noopener noreferrer"} **Théroux-Rancourt G\***, Earles JM\*, Gilbert ME, Zwieniecki MA, Boyce CK, McElrone A, Brodersen C (2017)*New Phytologist* 215(4), 1609–1622. doi: 10.1111/nph.14687

[Temperature gradients assist carbohydrate allocation within trees.](http://rdcu.be/to42){:target="_blank"  rel="noopener noreferrer"} Sperling O, Silva L, Tixier A, **Théroux-Rancourt G**, Zwieniecki MA (2017) *Scientific Reports* 7, 3265. doi: 10.1038/s41598-017-03608-w

[Excess diffuse light absorption in upper mesophyll limits CO<sub>2</sub> drawdown and depresses photosynthesis.](http://www.plantphysiol.org/content/early/2017/04/21/pp.17.00223.abstract){:target="_blank"  rel="noopener noreferrer"} Earles JM, **Théroux-Rancourt G**, Gilbert ME, McElrone A, Brodersen C (2017) *Plant Physiology* doi: 10.1104/pp.17.00223

[The light response of mesophyll conductance is controlled by structure across leaf profiles.](http://escholarship.org/uc/item/6cf1488m#page-3){:target="_blank"  rel="noopener noreferrer"} **Théroux-Rancourt G**, Gilbert ME (2017) *Plant, Cell & Environment* 40, 726–740. doi: 10.1111/pce.12890

[Greater efficiency of water use in poplar clones having a delayed response of mesophyll conductance to drought.](https://academic.oup.com/treephys/article/35/2/172/1622488/Greater-efficiency-of-water-use-in-poplar-clones){:target="_blank"  rel="noopener noreferrer"} **Théroux-Rancourt G**, Éthier G, Pepin S (2015) *Tree Physiology* 35, 172–184.

[Threshold response of mesophyll CO<sub>2</sub> conductance to leaf hydraulics in highly transpiring hybrid poplar clones exposed to soil drying.](https://academic.oup.com/jxb/article/65/2/741/488160/Threshold-response-of-mesophyll-CO2-conductance-to){:target="_blank"  rel="noopener noreferrer"} **Théroux-Rancourt G**, Éthier G, Pepin S (2014) *Journal of Experimental Botany* 65, 741–753.

[Evaluating fluxes in Histosols for water management in lettuce: a comparison of mass balance, evapotranspiration and lysimeter methods.](http://www.sciencedirect.com/science/article/pii/S0378377414000031){:target="_blank"  rel="noopener noreferrer"} Lafond J, Bergeron Piette E, Caron J, **Théroux-Rancourt G** (2014)*Agricultural Water Management* 135, 73–83.

[Optimal irrigation for onion, celery production and spinach seeds germination in histosols.](https://dl.sciencesocieties.org/publications/aj/abstracts/106/3/981){:target="_blank"  rel="noopener noreferrer"} Rekika D, Caron J, **Théroux-Rancourt G**, Lafond J, Jenni S, Gosselin A (2014)*Agronomy Journal 106*, 981–996.

[Nitrogen budget for fertilized carrot cropping systems in a Quebec organic soil.](http://www.nrcresearchpress.com/doi/full/10.4141/cjss2013-104#.WTrtV8lFdmM){:target="_blank"  rel="noopener noreferrer"} Caron J, **Théroux-Rancourt G**, Bélec C, Tremblay N, Parent LE (2014)*Canadian Journal of Soil Science 94*, 139–148

[Assessment of Methods to Determine the Loss of Water and Nitrate in a Lettuce Crop Grown in Organic Soil.](http://www.ishs.org/ishs-article/889_50){:target="_blank"  rel="noopener noreferrer"} Bergeron Piette E, Caron J, **Théroux-Rancourt G**, Rekika D, Gosselin A, Parent LE (2011) *Acta horticulturae 889*, 403–408.

[Cloudberry cultivation in cut-over peatlands: hydrological and soil physical impacts on the growth of different clones and cultivars.](http://mires-and-peat.net/pages/volumes/map05/map0506.php){:target="_blank"  rel="noopener noreferrer"} **Théroux-Rancourt G**, Rochefort L, Lapointe L (2009) *Mires and Peat 5*, 06, 1-16.


## Book chapters

[Abiotic Stress.](https://link.springer.com/chapter/10.1007/7397_2016_13){:target="_blank"  rel="noopener noreferrer"} Bastiaanse H, **Théroux-Rancourt G**, Tixier A (2017) In: Comparative and Evolutionary Genomics of Angiosperm Trees. Eds.: A Groover, Q Cronk. Springer. ISBN 978-3-319-49329-9. doi: 10.1007/7397_2016_13

[Cloudberry. In: Production of berries in peatland, 2nd edition.](http://www.gret-perg.ulaval.ca/uploads/tx_centrerecherche/GUIDE_Berries_en_2009_03.pdf){:target="_blank"  rel="noopener noreferrer"} Bellemare M, **Théroux-Rancourt G**, Lapointe L, Rochefort L (2009) Peatland Ecology Research Group. Université Laval, Québec. p. 3-44.


## Theses

[Relations entre la conductance du mésophylle au CO<sub>2</sub> et l'hydraulique des tiges et des feuilles chez des clones de peupliers hybrides variant en sensibilité à la sécheresse.](http://hdl.handle.net/20.500.11794/25571){:target="_blank"  rel="noopener noreferrer"} Ph.D. Thesis. Université Laval.

[Influence de l’hydrologie, du substratum et de la restauration d’une tourbière abandonnée sur la croissance de la chicouté.](http://hdl.handle.net/20.500.11794/19663){:target="_blank"  rel="noopener noreferrer"} M.Sc. Thesis. Université Laval.
