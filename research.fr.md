---
layout: default
permalink: /research/
title: "Projets"
lang: fr
---

Les feuilles des plantes sont des composantes clés des cycles du carbone et de l'eau à l'échelle du globe. Practiquement tout le carbone provenant de l'atmosphère vers les écosystèmes terrestres transite par les feuilles, tout comme eviron 70% de l'évapotranspiration. La recherche en écophysiologie a principalement mis l'accent sur les stomates et les cellules du mésophylle et leurs réactions aux changements de conditions environnementales. Toutefois, il y a un grand volume vide à l'intérieur des feuilles, l'espaces intercellulaire entre les stomates et les cellules du mésophylle, et cet espace a été que très peu étudié jusqu'à maintenant.
Mes travaux actuels portent sur la relation entre la structure et le fonctionnement des feuilles. J'étudie l'anatomie des feuilles, tant en deux qu'en trois dimensions, le tout en relation avec la photosynthèse, la transpiration, et les processus diffusifs entre les stomates et les chloroplastes. Ces travaux combinent anatomie et mesures écophysiologiques, en plus de modélisations et autres analyses informatisées.

Vous trouverez ici une sélection de publications de mes deux champs de recherches principaux, tout comme un aperçu d'autres projets où j'ai été impliqué. La liste complète de mes publications, avec liens vers le texte, se retrouve dans la page [_Publications_](/publications/).


<!-- Automated list generator -->

{% for theme in site.data.projects %}
<h2>{{ theme.title[site.active_lang] }}</h2>
{% for project in theme.subsection %}
<div class="toc">
<img src="{{ project.avatar }}" alt="{{ project.paper | smartify }}" class="avatar" style="width:200px" />
<a href="{{ project.url }}">{{ project.paper | markdownify }}</a>
<p>{% if site.active_lang == "en" %}{{ project.snippet.en | markdownify }}{% else %}{{ project.snippet.fr | markdownify }}{% endif %}</p>
</div>
{% endfor %}
{% endfor %}
