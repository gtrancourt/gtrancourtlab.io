---
layout: page
permalink: /about/
title: "About me"
show_title: false
lang: en
---

{% include image.html url="/images/GTR-Palmenhaus-400px.jpg" width=200 style="float:right" %}

I am group leader of the indoor farming research group at [Biopterre](https://biopterre.com){:target="_blank"  rel="noopener noreferrer"}, in the province of Québec, Canada. My group works on smart cultivation techniques (use of AI in decision making, indoor cultivation, use of LEDs) and on the production of secodnary metabolites in plants (cannabis and plants for the pharmaceutical industry). 

I have been until recently affiliated with the plant ecophysiology group at the [Institute of Botany](http://www.dib.boku.ac.at/institut-fuer-botanik-botany/){:target="_blank"  rel="noopener noreferrer"} of the [University of Natural Ressources and Life Sciences (BOKU Wien)](http://boku.ac.at){:target="_blank"  rel="noopener noreferrer"}, in Vienna, Austria, where I was the principal investigator of a [WWTF](wwtf.at){:target="_blank"  rel="noopener noreferrer"} funded project on the [dynamic anatomy of plant leaves](https://wwtf.at/programmes/life_sciences/#LS19){:target="_blank"  rel="noopener noreferrer"}. I was until recently funded  through a [Lise-Meitner](https://en.wikipedia.org/wiki/Lise_Meitner){:target="_blank"  rel="noopener noreferrer"} [Fellowship](http://www.fwf.ac.at/en/research-funding/fwf-programmes/meitner-programme/){:target="_blank"  rel="noopener noreferrer"} from the [Austrian Science Fund (FWF)](http://www.fwf.ac.at/en/){:target="_blank"  rel="noopener noreferrer"}. From 2014 to 2017, I was a [Katherine Esau postdoctoral fellow](http://www-plb.ucdavis.edu/esau/info/postdoc.htm) at the University of California, Davis, in the lab of [Matthew Gilbert](http://gilbertlab.ucdavis.edu/){:target="_blank"  rel="noopener noreferrer"}.


I'm currently working on understanding the physiological and structural basis of resource-use efficiency in plants, especially water and light use at the whole leaf level. My expertise is vast and my work is focused on basic and applied research in plant physiology, from growth cabinets to broad field experiments. My research has a strong technological component, using diverse techniques and building custom equipments to get quantitative measurements on plants and soil, in order to integrate into leaf, plant, or field level models.

My main collaborators of the past years have been [Danny Tholen (BOKU)](https://scholar.google.com/citations?user=iQUjOxAAAAAJ&hl=en&num=20&oi=ao){:target="_blank"  rel="noopener noreferrer"} and [Adam Roddy (Florida International University)](https://www.adamroddy.com/){:target="_blank"  rel="noopener noreferrer"}.


In the WWTF project I am leading, and I closely collaborating with:  
  [Ingeborg Lang (plant cell biologist, University of Vienna)](https://scholar.google.com/citations?user=8tzh90wAAAAJ&hl=en&num=20&oi=ao){:target="_blank"  rel="noopener noreferrer"}  
  [Walter Kropatsch and Jiří Hladůvka (pattern recongnition and image analysis, TU Wien)](https://www.prip.tuwien.ac.at/){:target="_blank"  rel="noopener noreferrer"}  
  [Anja Geitmann (plant cell biologist, McGill University)](https://www.plantbiomechanics.net/){:target="_blank"  rel="noopener noreferrer"}  
  [Anne Bonnin (radiation physicist, Swiss Light Source)](https://www.psi.ch/en/x-ray-tomography-group/people/anne-bonnin){:target="_blank"  rel="noopener noreferrer"}
  
  
<!-- My other active collaborators are:  
  [Carlos Herrera, BOKU Wien](https://scholar.google.com/citations?user=lBAuLl0AAAAJ&hl=en&num=20&oi=sra)  
   [Craig Brodersen, Yale University](http://campuspress.yale.edu/brodersenlab/)  
   [Mason Earles, UC Davis](http://jmearles.net/)  
   [Elisabeth Forrestel, UC Davis](https://scholar.google.com/citations?user=w8sjBOcAAAAJ&hl=en&num=20&oi=ao)  
   [Mina Momayyezi, UC Davis](https://scholar.google.com/citations?user=IcktHCsAAAAJ&hl=en&num=20&oi=ao)  
   [Chris Muir, U. of Hawai'i](http://cdmuir.netlify.com/)  
   [Kevin Simonin, San Francisco State University](https://www.kevinsimonin.com/) 


And not to forget recent and past collaborators:  
   [Kevin Boyce, Stanford University](https://earth.stanford.edu/kevin-boyce)  
   [Matthew Gilbert, UC Davis](http://gilbertlab.ucdavis.edu/)  
   [Or Sperling, ARO-Volcani Center, Gilat, Israel](http://www.agri.gov.il/en/people/1254.aspx)  
   [Maciej Zwieniecki, UC Davis](http://www.plantsciences.ucdavis.edu/plantsciences_faculty/zwieniecki/)  
   Gilbert Éthier, Université Laval  
  [Steeve Pepin, Université Laval](https://scholar.google.com/citations?hl=en&user=pvl_4L8AAAAJ)  
   [Line Rochefort, Université Laval](http://www.gret-perg.ulaval.ca/)  
   [Line Lapointe, Université Laval](https://scholar.google.com/citations?hl=en&user=GtCAwxgAAAAJ)
 -->