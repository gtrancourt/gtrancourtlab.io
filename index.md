---
layout: default
permalink: /
title: "Guillaume Théroux-Rancourt"
show_title: false
lang: en
---


{% include image.html url="/images/banner-20170609.jpg" width=1000 align="center" %}

Plant ecophysiologist and structural biologist. I am group leader of the indoor farming research group at [Biopterre](https://biopterre.com){:target="_blank"  rel="noopener noreferrer"}, in the province of Québec, Canada. My group works on smart cultivation techniques (use of AI in decision making, indoor cultivation, use of LEDs) and on the production of secodnary metabolites in plants (cannabis and plants for the pharmaceutical industry). I was also until recently affiliated with the Institute of Botany, University of Natural Resources and Life Sciences, Vienna (BOKU Wien), Austria. 


# Recent papers

>[Legacy effects of past water abundance shape trees 7-years later.](https://doi.org/10.1002/ajb2.16452){:target="_blank"  rel="noopener noreferrer"} Chin ARO, Gessler A, Lain-Lacosta O, Østerlund I, Schaub M, **Théroux-Rancourt G**, Voggeneder K, Hille Ris Lambers J. (2025)  *American Journal of Botany* e16452. [*free full text*](https://onlinelibrary.wiley.com/share/author/WEQ2XNRHJWKQK78CJJWT?target=10.1002/ajb2.16452){:target="_blank"  rel="noopener noreferrer"}

>[Into the spongy-verse: structural differences between leaf and flower mesophyll.](https://doi.org/10.1093/icb/icae154){:target="_blank"  rel="noopener noreferrer"} Schreel JDM, **Théroux-Rancourt G**, Diggle PK, Brodersen CR, Roddy AB. (2024) *Integrative and Comparative Biology* Icae154.

>[Linking foliar structure to function: why and how.](https://doi.org/10.1071/FP24150){:target="_blank"  rel="noopener noreferrer"} (2024) Schreel JDM, **Théroux-Rancourt G**, Roddy AB.*Functional Plant Biology* 51, FP24150.


>[Analyzing anatomy over three dimensions unpacks the differences in mesophyll diffusive area between sun and shade Vitis vinifera leaves.](https://academic.oup.com/aobpla/advance-article/doi/10.1093/aobpla/plad001/7005251){:target="_blank"  rel="noopener noreferrer"} **Théroux-Rancourt G**, Herrera C, Voggeneder K, Luijken N, Nocker L, Savi T, Scheffknecht S, Schneck M, Tholen D. (2023) *AoB Plants* 15, plad001.



<!-- ## Upcoming conference -->

<!-- ## Recent posts

<nav>
	<ul>
	{% for post in site.posts limit:5 %}
	  {% include postlink.html %}
	{% endfor %}
	</ul>
</nav> -->
