My academic website
===================

This site was built by customizing [Steven V. Miller's theme](https://github.com/svmiller/steve-ngvb-jekyll-template) and following his detailled instructions on [how to build a Jekyll site for academics](http://svmiller.com/blog/2015/08/create-your-website-in-jekyll/). The [Research](https://gtrancourt.github.io/research/) page layout was inspired by [Matthew Lincoln's](http://matthewlincoln.net/) theme for his [research](http://matthewlincoln.net/projects) page. Some style edits have been done following the [Much-Worse theme](https://github.com/gchauras/much-worse-jekyll-theme).

My website is also bilingual (french and english), and this was done using the internationalization plugin [Polyglot](https://github.com/untra/polyglot).

Feel free to use my attempt at a jekyll theme.
