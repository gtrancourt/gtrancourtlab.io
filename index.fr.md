---
layout: default
permalink: /
title: "Guillaume Théroux-Rancourt"
show_title: false
lang: fr
---


{% include image.html url="/images/banner-20170609.jpg" width=1000 align="center" %}

Je suis écophysiologiste spécialiste des relations entre la structure des plantes et leurs fonctions. Je suis directeur de l'équipe de recherche en cultures intérieure et cultures intelligentes et chercheur chez [Biopterre](https://biopterre.com){:target="_blank"  rel="noopener noreferrer"}. Mon équipe travaille sur les sytèmes de culture intelligents (agrophotonique, culture en milieu fermé, utilisation de l'IA) et sur la production de métabolites secondaires dans les plantes (culture de cannabis et molécules pour l'industrie pharmaceutique). J'ai été affilié jusqu'à récemment à l'Institut de la botanique de l'Université des ressources naturelles et sciences de la vie (BOKU Wien), à Vienne en Autriche. 


# Articles récents

>[Legacy effects of past water abundance shape trees 7-years later.](https://doi.org/10.1002/ajb2.16452){:target="_blank"  rel="noopener noreferrer"} Chin ARO, Gessler A, Lain-Lacosta O, Østerlund I, Schaub M, **Théroux-Rancourt G**, Voggeneder K, Hille Ris Lambers J. (2025)  *American Journal of Botany* e16452. [*accès gratuit*](https://onlinelibrary.wiley.com/share/author/WEQ2XNRHJWKQK78CJJWT?target=10.1002/ajb2.16452){:target="_blank"  rel="noopener noreferrer"}

>[Into the spongy-verse: structural differences between leaf and flower mesophyll.](https://doi.org/10.1093/icb/icae154){:target="_blank"  rel="noopener noreferrer"} Schreel JDM, **Théroux-Rancourt G**, Diggle PK, Brodersen CR, Roddy AB. (2024) *Integrative and Comparative Biology* Icae154.

>[Linking foliar structure to function: why and how.](https://doi.org/10.1071/FP24150){:target="_blank"  rel="noopener noreferrer"} (2024) Schreel JDM, **Théroux-Rancourt G**, Roddy AB.*Functional Plant Biology* 51, FP24150.

>[Analyzing anatomy over three dimensions unpacks the differences in mesophyll diffusive area between sun and shade Vitis vinifera leaves.](https://academic.oup.com/aobpla/advance-article/doi/10.1093/aobpla/plad001/7005251){:target="_blank"  rel="noopener noreferrer"} **Théroux-Rancourt G**, Herrera C, Voggeneder K, Luijken N, Nocker L, Savi T, Scheffknecht S, Schneck M, Tholen D. (2023) *AoB Plants* 15, plad001.

<!-- ## Upcoming conference -->

<!-- ## Blogue (en anglais seulement)

<nav>
	<ul>
	{% for post in site.posts limit:5 %}
	  {% include postlink.html %}
	{% endfor %}
	</ul>
</nav> -->
